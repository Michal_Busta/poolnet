## A Simple Pooling-Based Design for Real-Time Salient Object Detection


## Prerequisites

- [Pytorch 1.5](http://pytorch.org/)
- [torchvision](http://pytorch.org/)
- opencv-python

 
## Simple setup for python3 

 - download python miniconda https://docs.conda.io/en/latest/miniconda.html 
 - conda install pytorch torchvision cudatoolkit=10.2 -c pytorch
 - pip install opencv-python
 
## Simple setup for python3 withot GPU

 - download python miniconda https://docs.conda.io/en/latest/miniconda.html 
 - conda install pytorch torchvision cpuonly -c pytorch
 - pip install opencv-python
 
## Usage 
 - put some jpg images to a data subfolder 
 - for a demo, run: python process_saliency.py
 
## Usage in a script 

Load a model (just once at beggining)

```
from process_saliency import load_model, process_image
model = load_model(use_gpu = use_gpu)  
```

read a image and call processing function: 

```
img = cv2.imread(img_name)
img_sal, cut, sal2, cut2 = process_image(model, img_name, use_gpu=use_gpu)
```

The function will just call a network for salient object detection in two steps - detect a object in whole image (result is cut) 
and repeats a object detection on cutted image (cut2 is the final result)

## TODO 
 - retrain network for small objects (and remove 2 stage processing)



