'''
Created on Jul 13, 2020

@author: michal.busta at gmail.com
'''

import cv2
import os
import glob
import numpy as np

import torch

from networks.poolnet import PoolNet, extra_layer, resnet50_locate

def get_saliency(model, imr, use_gpu = True):
  '''
  Returns a saliency segmentation 
  '''

  torch.cuda.empty_cache()
  in_ = np.array(imr, dtype=np.float32)
  im_size = tuple(in_.shape[:2])
  in_ -= np.array((104.00699, 116.66877, 122.67892))
  in_ = in_.transpose((2,0,1))
  image_t = torch.Tensor(in_)
  with torch.no_grad():
    images = torch.autograd.Variable(image_t.unsqueeze(0))
    if use_gpu:
      images = images.cuda()
    preds = model(images)
    pred = np.squeeze(torch.sigmoid(preds).cpu().data.numpy())
    multi_fuse = 255 * pred
    multi_fuse = multi_fuse.astype(np.uint8)
    images = None
    preds = None
    
    return multi_fuse
  

def process_image(model, img_path, use_gpu = True):
  '''
  @returns  multi_fuse, cut, sal2, cut2  
   where:
    multi_fuse is saliency map of whole image 
    cut - is first stage cutted image (works well for a big objects)
    sal2 - saliency map of a cut image 
    cut - is second stage cutted image (cut from a cut image )
  '''
  
  im = cv2.imread(img_path)
  scale1 = 1
  if im.shape[0] > 640 or im.shape[1] > 640:
    scale1 = 4
    imr = cv2.resize(im, (im.shape[1] // scale1, im.shape[0] // scale1))
  else:
    imr = im
    
  multi_fuse = get_saliency(model, imr, use_gpu)
  
  threshold = cv2.threshold(multi_fuse, 200,255,cv2.THRESH_BINARY)[1]
  nz = threshold.nonzero()
  if nz[1].sum() == 0:
    return multi_fuse, im
  rect = [nz[1].min() * scale1, nz[0].min()* scale1, nz[1].max()* scale1, nz[0].max()* scale1]  
  
  try:
    offset = 20 
    min_y = rect[1]-offset
    min_y = max(0, min_y)
    min_x = rect[0] - offset
    min_x = max(0, min_x)
    cut = img[min_y: rect[3] + 2 * offset, min_x:rect[2] + 2 * offset, :]
  except:
    cut = img[rect[1]: rect[3], rect[0]:rect[2], :]
  
  return multi_fuse, cut
  
  '''
  try:
    scale = 1
    if cut.shape[0] > 640 or cut.shape[1] > 640:
      cutr = cv2.resize(cut, (cut.shape[1] // 2, cut.shape[0] // 2))
      scale = 2
    else:
      cutr = cut
    
    sal2 = get_saliency(model, cutr, use_gpu)
    
    threshold = cv2.threshold(sal2, 200,255,cv2.THRESH_BINARY)[1]
    nz = threshold.nonzero()
    rect = [nz[1].min() * scale, nz[0].min() * scale, nz[1].max() * scale, nz[0].max() * scale]  
    offset = 50 
    min_y = rect[1]-offset
    min_y = max(0, min_y)
    min_x = rect[0] - offset
    min_x = max(0, min_x)
    
    cut2 = cut[min_y: rect[3] + 2*offset, min_x:rect[2] + 2 * offset, :]
  except:
    return multi_fuse, cut, multi_fuse, cut
    
  return multi_fuse, cut, sal2, cut2
  '''

def load_model(model_path = 'final.pth', use_gpu=True):  
  
  base_model_cfg = 'resnet'  
  model_path = 'final.pth'
  #model_path = '/home/busta/Downloads/epoch_3.pth'
  #model_path = '/home/busta/Downloads/pool_last.pth'
  model = PoolNet(base_model_cfg, *extra_layer(base_model_cfg, resnet50_locate()))
  if use_gpu:
    model = model.cuda()
    model.load_state_dict(torch.load(model_path))
  else:  
    model.load_state_dict(torch.load(model_path, map_location='cpu'))
   
  model.eval()
  return model

if __name__ == '__main__':
  
  use_gpu = torch.cuda.is_available()
  #use_gpu = False
  
  ##loading 
  model = load_model(use_gpu = use_gpu)  
  
  out_dir2 = 'result_cut'
  if not os.path.exists(out_dir2):
    os.mkdir(out_dir2)
    
  if not os.path.exists('bad'):
    os.mkdir('bad')
    
  file2 = open("{0}/index.html".format(out_dir2),"w") 
  
  file2.write('<table>\n')
  cnt = 0
  for img_name in glob.glob('data/*.jpg'):
  #for img_name in glob.glob('/home/busta/Downloads/ipsp/*.jpg'):
  #for img_name in glob.glob('/home/busta/git/PoolNet/data/DUTS/DUTS-TR/bad/*jpg'):
    
    cnt += 1
    #if cnt < 600:
    #  continue
    
    #img_name = '/home/busta/Downloads/ipsp/925835_01_qual85.jpg'
     
    img = cv2.imread(img_name)
    base_name = os.path.basename(img_name)
    img_sal, cut = process_image(model, img_name, use_gpu=use_gpu)
    sal_base = '{0}_sal.png'.format(base_name[:-4])
    cv2.imwrite('{0}/{1}'.format(out_dir2, sal_base), img_sal)
    imr = cv2.resize(img, (img.shape[1] // 4, img.shape[0]//4), interpolation=cv2.INTER_CUBIC)
    
    cv2.imshow('img', img)
    cv2.imshow('img_sal', img_sal)   
    #cv2.imshow('img_sal2', sal2)    
    cv2.imshow('cut', cut)
    #cv2.imshow('cut2', cut2)
    
    
    cutr = cv2.resize(cut, (cut.shape[1] // 2, cut.shape[0] // 2))  
    file2.write('<tr><td><img src="{0}" /></td><td><img src="{1}" /></td><td><img src="{2}_cut.png" /></td></tr>\n'.format(base_name, sal_base, base_name[:-4]))
    
    cv2.imwrite('{0}/{1}_cut.png'.format(out_dir2, base_name[:-4]), cutr)
    cv2.imwrite('{0}/{1}'.format(out_dir2, base_name), imr)
    #cv2.imwrite(f'{out_dir2}/{sal_base}', sal2)
    
    key = cv2.waitKey(0)
    if key == ord('b'):
      cv2.imwrite('bad/{0}'.format(base_name), imr)
      img_salr = cv2.resize(img_sal, (imr.shape[1], imr.shape[0]))
      cv2.imwrite('bad/{0}_sal.png'.format(base_name[:-4]), img_salr)
      
    
    
    #if cnt > 700:
    #  break
      
  file2.write('</table>\n')
  
  
    